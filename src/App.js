import React from 'react';
import logo from './logo.svg';
import Users from './components/Users'
import './App.css';
import useFetchData from './hooks/fetchData';

function App() {
    const {
        data,
        loading
    } = useFetchData('https://randomuser.me/api/?results=10')

    return (
        <div className="App">
            <header className="App-header">
            { loading
                ? (
                    <>
                        <h1>Loading...</h1>
                        <img src={logo} className="App-logo" alt="logo" />
                    </>
                )
                : (
                    <Users
                        data={ data }
                    />
                )
            }
            </header>
        </div>
    );
}

export default App;
