const capitalize = text => `${text.charAt(0).toUpperCase()}${text.slice(1)}`

const capitalizeName = ({
    title,
    first,
    last
}) =>  `${capitalize(title)} ${capitalize(first)} ${capitalize(last)}` 

export {
    capitalize,
    capitalizeName
}
