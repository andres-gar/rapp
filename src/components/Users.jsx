import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import User from './User';
import { capitalizeName } from '../utils/text'

function Users ({ data }) {
    const [view, setView] = useState(null)

    const setCurrentUser = useCallback(email => {
        const current = data.findIndex(({ email: prev }) => email === prev)
        setView(current)
    }, [data])

    const _handleResetView = useCallback(() => {
        setView(null)
    }, [])

    if (!data) return null

    if (view != null) {
        const user = data[view]
        return (
            <div>
                <span
                    onClick={ _handleResetView }
                    style={{
                        cursor: 'pointer',
                        right: '30%',
                        position: 'absolute',
                        top: '30%'
                    }}
                >
                    X
                </span>
                <p>{ capitalizeName(user.name) }</p>
                <p>{ user.email }</p>
                <p>{ user.phone }</p>
            </div>
        )
    }

    return (
        <>
            <h1>Users</h1>
            <ul>
                { data.map(({
                    name: {
                        title,
                        first,
                        last
                    },
                    email
                }, i) => (
                    <li
                        key={ `${title}_${first}_${last}` }
                    >
                        <User
                            info={ data[i] }
                            setCurrentUser={ setCurrentUser }
                        />
                    </li>
                )) }
            </ul>
        </>
    )
}

Users.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object)
}

Users.defaultProps = {
    data: null
}

export default Users
