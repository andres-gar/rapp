import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { capitalizeName } from '../utils/text'

function User ({
    info: {
        name,
        email
    },
    setCurrentUser
}) {
    const _handleClickUser = useCallback(() => {
        setCurrentUser(email)
    }, [email, setCurrentUser])

    return (
        <div
            onClick={ _handleClickUser }
            style={{
                cursor: 'pointer'
            }}
        >
            <span>{ capitalizeName(name) }</span>
        </div>
    )
}

User.propTypes = {
    info: PropTypes.object.isRequired,
    setCurrentUser: PropTypes.func.isRequired
}

export default User
