import { useState, useEffect } from 'react'
import axios from 'axios'

function useFetchData (api) {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(null)

    useEffect(() => {
        async function body () {
            const {
                data: {
                    results
                }
            } = await axios.get(api)
            setData(results)
            setLoading(false)
        }
        body()
    }, [api])
    
    return {
        data,
        loading
    }
}

export default useFetchData
